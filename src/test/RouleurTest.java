package test;

import static org.junit.Assert.*;

import org.junit.Test;

import Projet.Rouleur;

/**
* @author HOU Yunan
*
*/
public class RouleurTest {

	@Test
	public void testRouleur() {
		Rouleur r=new Rouleur();
		assertEquals ("Le nom devrait etre null ",r.getNomJoueur(),null);
		Rouleur r1=new Rouleur();
		assertTrue("Les deux doit etre les memes ",r.equal(r1));
	}


}
