package test;



import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Projet.CarteFatique;

/**
* @author HOU Yunan
* 
*/
public class CarteFatiqueTest {

	@Test
	public void testCarteFatique() {

		CarteFatique cf=new CarteFatique(2);
		assertEquals ("Le numero devrait etre 2 ",cf.getNumero(),2);
	}

}
