package test;

import static org.junit.Assert.*;

import org.junit.Test;

import Projet.Casse;
import Projet.Rouleur;

/**
* @author HOU Yunan
* 
*/
public class CasseTest {

	@Test
	public void testCasse() {
		Casse cas=new Casse();
		assertEquals ("Le gauche devrait etre null ",cas.getGauche(),null);
		assertEquals("Le doite devrait etre null ",cas.getDroite(),null);
		cas.setGauche(new Rouleur());
		cas.setDroite(new Rouleur());
		assertEquals ("Le gauche ne devrait pas etre null ",cas.getGauche().getPositionActuelle(),0);
		assertEquals ("Le doite ne devrait pas etre null ",cas.getDroite().getPositionActuelle(),0);
		cas.clearDroite();
		cas.clearGauche();
		assertEquals ("Le gauche devrait etre null ",cas.getGauche(),null);
		assertEquals("Le doite devrait etre null ",cas.getDroite(),null);
	}

}
