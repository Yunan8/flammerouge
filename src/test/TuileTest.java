package test;

import static org.junit.Assert.*;

import org.junit.Test;

import Projet.Tuile;

/**
* @author HOU Yunan
* 
*/
public class TuileTest {

	@Test
	public void testTuile() {
		Tuile t=new Tuile("a",0,0);
		assertFalse("le pente devrait etre false",t.estDescente());
		assertTrue("le pente devrait etre true",t.estLigneDroite());
	}

}
