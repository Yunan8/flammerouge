package test;

import static org.junit.Assert.*;

import org.junit.Test;

import Projet.CarteEnergie;

/**
* @author HOU Yunan
*
*/
public class CarteEnergieTest {

	@Test
	public void testCarteEnergie() {
		CarteEnergie cf=new CarteEnergie(2);
		assertEquals ("Le numero devrait etre 2 ",cf.getNumero(),2);
	}

}
