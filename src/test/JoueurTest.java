package test;

import static org.junit.Assert.*;

import org.junit.Test;

import Projet.Joueur;

/**
* @author HOU Yunan
* 
*/
public class JoueurTest {

	@Test
	public void testJoueur() {
		Joueur j=new Joueur();
		assertEquals ("Le nom devrait etre null ",j.getNomJoueur(),null);
		j.setNomJoueur("a");
		assertEquals ("Le nom devrait etre a ",j.getNomJoueur(),"a");
	}
}
