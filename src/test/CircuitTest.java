package test;

import static org.junit.Assert.*;


import org.junit.Test;

import Projet.Circuit;
import Projet.Tuile;

/**
* @author HOU Yunan
* 
*/
public class CircuitTest {
	Circuit avenue;
	@Test
	public void testCircuit() {
		avenue= new Circuit("Avenue Corso Paseo");
		avenue.setCircuit();
		assertEquals ("Le nom devrait etre correct ",avenue.getNomCircuit(),"Avenue Corso Paseo");
		assertEquals ("Le size devrait etre 21 ",avenue.getCircuitmain().size(),21);
		Tuile t=avenue.tuilleALaPosition(0);
		assertEquals ("Le nom devrait etre a ",t.getnomTuile(),"a");
		assertFalse("la tuile doit etre normale" ,avenue.positionEnDescente(0));
		assertFalse("la tuile doit etre normale" ,avenue.positionEnMontee("a"));
		assertTrue("la tuile doit etre normale" ,avenue.positionEnVoieNormale(0));
	}
}
