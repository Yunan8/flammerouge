package Projet;

public class CarteEnergie extends Carte {
	public CarteEnergie(int n) {
		super(n);
	}

	@Override
	public String toString() {
		return "CarteEnergie [numero=" + numero + "]";
	}
	
}
