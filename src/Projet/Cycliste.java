package Projet;

import java.util.*;

/**
 * @author HOU Yunan
 * 
 */
public abstract class Cycliste{
	private String nomJoueur;
	private int positionActuelle;
	private String nom;
	private boolean fileDroite;
	public List<Carte> carte;
	private List<Carte> carteFaceCachee;
	private int absolute;

	/**
	 * constructeur pour creer une nouvelle cycliste
	 * 
	 * test pass��
	 */
	public Cycliste() {
		this.nom = null;
		this.positionActuelle = 0;
		// inistialiser les 4 cartes
		carteFaceCachee = new ArrayList<Carte>();

		// initialiser le defausse
		carte = new ArrayList<Carte>();

	}

	/**
	 * methode inserer les cartes
	 */
	public abstract void insererCartes();

	/**
	 * methode pour choisir 4 carte et remove les 4 cartes
	 * 
	 * @return test pass��
	 */
	private List<Carte> piocher4Cartes() {
		carteFaceCachee = new ArrayList<Carte>();
		if (carte.size() >= 4) {
			for (int i = 0; i < 4; i++) {
				carteFaceCachee.add(carte.get(0));
				carte.remove(0);
			}
		} else {
			this.carteFaceCachee = this.carte;
		}
		return this.carteFaceCachee;
	}

	/**
	 * choisir dans les 4 cartes et jetter 1 carte
	 * 
	 * test pass��
	 */
	@SuppressWarnings("resource")
	public void choisirCarte() {
		this.piocher4Cartes();
		// des chosses sur la graphique pour choisir
		Scanner sc = new Scanner(System.in);
		String s1 = " _______________     ";
		String s2 = "|  ___________  |    ";
		String s3 = "| |           | |    ";
		String s5 = "| |___________| |    ";
		String s6 = "|_______________|    ";
		String l1 = "     Carte ", l2 = "         ";
		String p1 = "| |     ";
		String p2 = "     | |    ";
		String a1 = "", a2 = "", a3 = "", a5 = "", a6 = "", a4 = "", a7 = "";
		for (int j = 0; j < carteFaceCachee.size(); j++) {
			a1 = a1 + s1;
			a2 = a2 + s2;
			a3 = a3 + s3;
			a5 = a5 + s5;
			a6 = a6 + s6;
			a4 = a4 + p1 + carteFaceCachee.get(j).getNumero() + p2;
			a7 = a7 + l1 + j + l2;
		}
		System.out.println(a1);
		System.out.println(a2);
		System.out.println(a3);
		System.out.println(a3);
		System.out.println(a3);
		System.out.println(a4);
		System.out.println(a3);
		System.out.println(a3);
		System.out.println(a5);
		System.out.println(a6);
		System.out.println(a7);
		System.out.println(
				"Choisir votre catre dans les 4(tapez une chiffre entre 0 et " + (carteFaceCachee.size() - 1) + ") :");

		int place = 0;
		boolean correct = false;
		while (!correct) {
			try {
				sc = new Scanner(System.in);
				place = sc.nextInt();
				correct = true;
				if (place < 0 || place > 3) {
					correct = false;
					throw new JeuException("tapez une entier entre 0 et 3 s.v.p :");

				}
			} catch (JeuException e) {
				System.out.println(e.getMessage());
			} catch (Exception e) {
				System.out.println("ce n'est pas une entier, tapez une entier s.v.p :");

			}
		}
		// confirmer la choix
		Carte temp = carteFaceCachee.get(place);
		for (int i = 0; i < carteFaceCachee.size(); i++) {
			if (i != place) {
				carte.add(carteFaceCachee.get(i));
			}
		}
		carteFaceCachee.clear();
		carteFaceCachee.add(temp);
	}

	/**
	 * la methode pour avancer cette cycliste et retourner la circuit qui a change
	 * 
	 * test passe
	 * 
	 * @param c
	 * @return
	 */
	public Circuit avancer(Circuit c) {
		Carte temp = carteFaceCachee.get(0);
		List<Tuile> tuilec = c.getCircuitmain();
		boolean trouve = false;
		int i = 0;
		while (!trouve && i < tuilec.size()) {
			Tuile temp1 = tuilec.get(i);
			if (temp1.getnomTuile().equals(this.nom)) {
				trouve = true;
			} else {
				i++;
			}
		}
		int index = i;
		Cycliste temp2 = c.getCycliste(i, this.positionActuelle, this.fileDroite);
		c.clearCycliste(i, this.positionActuelle, this.fileDroite);
		trouve = false;
		int placemove = 0;
		while (!trouve) {
			placemove = 0;
			i = 0;
			while (!trouve) {
				if (i < tuilec.size()) {
					Tuile temp1 = tuilec.get(i);
					placemove = placemove + temp1.getTuilecase().size();
					if (placemove >= (absolute + temp.getNumero())) {
						trouve = true;
						placemove = placemove - temp1.getTuilecase().size();
						this.positionActuelle = absolute - 1 + temp.getNumero() - placemove;
						this.nom = tuilec.get(i).getnomTuile();
					} else {
						i++;
					}
				} else {
					this.positionActuelle = 5;
					this.nom = tuilec.get(tuilec.size() - 1).getnomTuile();
					i--;
					trouve = true;
				}
			}
			if (c.positionEnVoieNormale(index)) {
				if (tuilec.get(i).estMontee()) {
					if (temp.getNumero() > 5) {
						this.nom = tuilec.get(i - 1).getnomTuile();
						this.positionActuelle = tuilec.get(i - 1).getTuilecase().size() - 1;
						i--;
					}
					System.out.println("la cycliste de " + this.nomJoueur + " approche une tuile montee ");

				}
			} else {
				if (c.positionEnDescente(index)) {
					if (temp.getNumero() < 5) {
						temp = new CarteEnergie(5);
						trouve = false;
					}
					System.out
							.println("attention : la cycliste de " + this.nomJoueur + " est dans une tuile descente ");

				} else {
					if (temp.getNumero() > 5) {
						temp = new CarteEnergie(5);
						trouve = false;
					}
					System.out.println("attention : la cycliste de " + this.nomJoueur + " est dans une tuile montee ");

				}
			}

			if (tuilec.get(i).getTuilecase().get(this.positionActuelle).getDroite() != null) {
				if (tuilec.get(i).getTuilecase().get(this.positionActuelle).getGauche() != null) {
					trouve = false;
					this.absolute--;
				} else {
					this.fileDroite = false;
				}
			} else {
				this.fileDroite = true;
			}
		}

		temp2.setposition(this.nom, this.positionActuelle, this.fileDroite, c);
		c.setCycliste(i, this.positionActuelle, this.fileDroite, temp2);
		this.setAbsolute(c);
		carteFaceCachee.clear();
		return c;
	}

	/**
	 * le methode pour verifier s'il y a une case vide avant cette cycliste test
	 * passe
	 * 
	 * @param c
	 * @return
	 */
	public boolean fatique(Circuit c) {
		List<Tuile> tuilec = c.getCircuitmain();
		boolean trouve = false;
		int i = 0;
		while (!trouve && i < tuilec.size()) {
			Tuile temp1 = tuilec.get(i);
			if (temp1.getnomTuile().equals(this.nom)) {
				trouve = true;
			} else {
				i++;
			}
		}
		trouve = false;
		int placemove = 0;
		i = 0;
		while (!trouve) {
			if (i < tuilec.size()) {
			Tuile temp1 = tuilec.get(i);
			placemove = placemove + temp1.getTuilecase().size();
			if (placemove >= (absolute + 1)) {
				trouve = true;
				placemove = placemove - temp1.getTuilecase().size();
			} else {
				i++;
			}
			}else {
				return false;
			}
		}
		if (c.getCycliste(i, absolute + 1 - placemove - 1, true) == null
				&& c.getCycliste(i, absolute + 1 - placemove - 1, false) == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * verifier si les deux cycliste sont le meme ou non
	 * 
	 * @param c
	 * @return
	 */
	public boolean equal(Cycliste c) {
		boolean equal = false;
		if (this.getNomJoueur() == c.getNomJoueur() && this.getNom() == c.getNom()
				&& this.getPositionActuelle() == c.getPositionActuelle() && this.isFileDroite() == c.isFileDroite()) {
			equal = true;
		}
		return equal;
	}

	/**
	 * avancer la position avec 1 pour appliquer l'aspiration
	 * 
	 * @param c
	 * @return
	 */
	public Circuit avanceraspiration(Circuit c) {
		List<Tuile> tuilec = c.getCircuitmain();
		boolean trouve = false;
		int i = 0;
		while (!trouve && i < tuilec.size()) {
			Tuile temp1 = tuilec.get(i);
			if (temp1.getnomTuile().equals(this.nom)) {
				trouve = true;
			} else {
				i++;
			}
		}
		System.out.println(this.getNom() + " " + this.getAbsolute() + " " + this.getPositionActuelle() + " "
				+ this.isFileDroite());
		Cycliste temp2 = c.getCycliste(i, this.positionActuelle, this.fileDroite);
		System.out.println(temp2.getNom() + " " + temp2.getAbsolute() + " " + temp2.getPositionActuelle() + " "
				+ temp2.isFileDroite());
		c.clearCycliste(i, this.positionActuelle, this.fileDroite);
		trouve = false;
		int placemove = 0;
		i = 0;
		while (!trouve) {
			Tuile temp1 = tuilec.get(i);
			placemove = placemove + temp1.getTuilecase().size();
			if (placemove >= (absolute + 1)) {
				trouve = true;
				placemove = placemove - temp1.getTuilecase().size();
			} else {
				i++;
			}
		}
		this.positionActuelle = absolute - 1 + 1 - placemove;
		this.nom = tuilec.get(i).getnomTuile();
		temp2.setposition(this.nom, this.positionActuelle, this.fileDroite, c);
		c.setCycliste(i, this.positionActuelle, this.fileDroite, temp2);
		this.setAbsolute(c);
		return c;
	}

	/**
	 * si le cyclsite est dqns le droite reourne true sinon false
	 * 
	 * @return
	 */
	public boolean isFileDroite() {
		return fileDroite;
	}

	/**
	 * mise a jour chaque fois les parametres d'un cycliste
	 * 
	 * @param n
	 * @param p
	 * @param d
	 * @param c
	 */
	public void setposition(String n, int p, boolean d, Circuit c) {
		this.nom = n;
		this.positionActuelle = p;
		this.fileDroite = d;
		this.setAbsolute(c);
	}

	/**
	 * ajouter carte fatique et melanger encore une fois
	 * 
	 * @param c
	 */
	public void ajoutercarte(Carte c) {
		this.carte.add(c);
	}

	/**
	 * mise a jour la position absolute pour cette cycliste
	 * 
	 * @param c
	 */
	public void setAbsolute(Circuit c) {
		List<Tuile> tuilec = c.getCircuitmain();
		boolean trouve = false;
		int i = 0;
		int a = 0;
		while (!trouve) {
			Tuile temp1 = tuilec.get(i);
			a = a + temp1.getTuilecase().size();
			if (temp1.getnomTuile().equals(this.nom)) {
				trouve = true;
				a = a - temp1.getTuilecase().size();
			} else {
				i++;
			}
		}
		a = a + this.positionActuelle + 1;
		this.absolute = a;
	}

	/**
	 * retourne la position absolute
	 * 
	 * @return
	 */
	public int getAbsolute() {
		return this.absolute;
	}

	/**
	 * retourne le nom de la tuile
	 * 
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * mettre le nom en parametre
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * retourne le nom du joueur
	 * 
	 * @return
	 */
	public String getNomJoueur() {
		return nomJoueur;
	}

	/**
	 * changer le nom du joueur
	 * 
	 * @param nomJoueur
	 */
	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}

	/**
	 * retourne la liste des cartes qui sont choisi
	 * 
	 * @return
	 */
	public List<Carte> getCarteFaceCachee() {
		return carteFaceCachee;
	}

	/**
	 * changer la liste des cartes qui sont choisi
	 * 
	 * @param carteFaceCachee
	 */
	public void setCarteFaceCachee(List<Carte> carteFaceCachee) {
		this.carteFaceCachee = carteFaceCachee;
	}

	/**
	 * retourne la position relative
	 * 
	 * @return
	 */
	public int getPositionActuelle() {
		return positionActuelle;
	}

	/**
	 * le moethode pour afficher si on applique fatique ou non
	 */
	public void afficher() {
		System.out.println("appliquer Fatique le cycliste de" + this.nomJoueur + "est dans le tuile " + this.nom
				+ " de la casse " + this.positionActuelle);
	}

	@Override
	public abstract String toString();
	
	
}
