package Projet;

import java.util.ArrayList;

/**
 * @author HOU Yunan
 * 
 */
public class Sprinter extends Cycliste {
	/**
	 * Constructeur
	 */
	public Sprinter() {
		super();
		this.insererCartes();

	}

	public void insererCartes() {
		carte = new ArrayList<Carte>();
		for (int i = 0; i < 3; i++) {
			carte.add(new CarteEnergie(2));
			carte.add(new CarteEnergie(3));
			carte.add(new CarteEnergie(4));
			carte.add(new CarteEnergie(5));
			carte.add(new CarteEnergie(9));
		}
		// deranger les cartes
		for (int i = 14; i > 0; i--) {
			int k = (int) (Math.random() * (i + 1));
			Carte temp = carte.get(i);
			carte.set(i, carte.get(k));
			carte.set(k, temp);
		}

	}

	@Override
	public String toString() {
		return "Sprinter [getNomJoueur()=" + getNomJoueur() + ", getNom()=" + getNom() + ", getPositionActuelle()="
				+ getPositionActuelle() + ", isFileDroite()=" + isFileDroite() + ", getAbsolute()=" + getAbsolute()
				+ "]";
	}
	
}
