package Projet;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author HOU Yunan
 * 
 */

@SuppressWarnings("serial")
public class Jeu extends JeuException {
	private List<Joueur> joueur;
	private List<Carte> carteFatiqueSprinter;
	private List<Carte> carteFatiqueRoleur;
	private Circuit avenue;
	private int tour;
	/**
	 * constructeur qui permet de initialiser les parametres et aussi initialiser le
	 * liste des joueurs
	 * 
	 */
	@SuppressWarnings("resource")
	public Jeu() {
		// initialiser les parametres
		joueur = new ArrayList<Joueur>();
		
		tour = 0;
		// initialiser les cartes fatiques
		int i = 0;
		

		// initialiser la table pour joueur
		int nombre = 0;
		Scanner sc = new Scanner(System.in);
		boolean correct = false;
		while (!correct) {
			System.out.println("_____la 1er etape_____");
			System.out.println("tapez le nombre de joueur(entre 2 et 4) s.v.p:");
			try {
				sc = new Scanner(System.in);
				nombre = sc.nextInt();
				correct = true;
				if (nombre < 2 || nombre > 4) {
					correct = false;
					throw new JeuException("tapez une entier entre 2 et 4 s.v.p");

				}
			} catch (JeuException e) {
				System.out.println(e.getMessage());
			} catch (Exception e) {
				System.out.println("ce n'est pas une entier, tapez une entier s.v.p");

			}
		}

		// initialiser le list vide
		i = 0;
		while (i < nombre) {
			joueur.add(new Joueur());
			i++;
		}
		// ajouter les valeurs
		i = 0;
		String nomJ;
		while (i < nombre) {
			System.out.print("tapez le nom pour " + i + "eme Joueur s.v.p:");
			nomJ = sc.next();
			Joueur ajouter = new Joueur();
			ajouter.setNomJoueur(nomJ);
			joueur.set(i, ajouter);
			i++;
		}
	}

	/**
	 * initialiser les positions des rouleurs et des sprinters
	 * 
	 */
	@SuppressWarnings("resource")
	public void positionDepart() {
		avenue = new Circuit("Avenue Corso Paseo");
		avenue.setCircuit();
		carteFatiqueSprinter = new ArrayList<Carte>();
		carteFatiqueRoleur = new ArrayList<Carte>();
		for (int i = 0; i < 60; i++) {
			carteFatiqueSprinter.add(new CarteFatique(2));
		}
		for (int i = 0; i < 60; i++) {
			carteFatiqueRoleur.add(new CarteFatique(2));
		}
		Scanner sc = new Scanner(System.in);
		// prendre la tuile depart
		Tuile t = avenue.tuilleALaPosition(0);
		// parcourir le list
		System.out.println();
		System.out.println();
		System.out.println("_____la 2eme etape_____");
		System.out.println();
		for (int i = 0; i < joueur.size(); i++) {

			Joueur j = joueur.get(i);

			// des instructions
			System.out.println("-----------------------------------------------------------------------------");
			System.out.println("| la condition des places occupees                                          |");
			System.out.println("| dans la tuile avec le nom 'a' qui est la tuile possedant une ligne depart |");
			System.out.println("| la ligne depart est entre casse 4 et casse 5                              |");
			System.out.println("-----------------------------------------------------------------------------");
			// initialiser une liste de case pour remplacer le tuile dans le avenue
			List<Casse> temp = t.getTuilecase();
			// 6 cases dans la tuile
			System.out.println(" _______________________________________________________");
			System.out.println("|case number    ||     gauche        |      doite       |");
			for (int m = 0; m < temp.size() - 1; m++) {
				// parcourir la liste des cases et verifier si c'est deja occupe
				Casse afficher = temp.get(m);
				String s = "|case " + m + "         || 0                 |0                 |";
				// condition:gauche et droit sont null
				if (afficher.getGauche() == null && afficher.getDroite() == null) {
					System.out.println(s);
				} else {
					// seulement la droite est null
					if (afficher.getGauche() == null) {
						String s1 = s.substring(0, 38);
						s1 = s1 + afficher.getDroite().getNomJoueur();
						while (s1.length() < 56) {
							s1 = s1 + " ";
						}
						s1 = s1 + "|";
						System.out.println(s1);
					} else {
						// la gauche est null
						if (afficher.getDroite() == null) {
							String s2 = s.substring(0, 18);
							s2 = s2 + afficher.getGauche().getNomJoueur();
							while (s2.length() < 37) {
								s2 = s2 + " ";
							}
							s2 = s2.substring(0, 37);
							s2 = s2 + "|0";
							while (s2.length() < 56) {
								s2 = s2 + " ";
							}
							s2 = s2 + "|";
							System.out.println(s2);
						} else {
							String s3 = s.substring(0, 18);
							s3 = s3 + afficher.getGauche().getNomJoueur();
							while (s3.length() < 37) {
								s3 = s3 + " ";
							}
							s3 = s3.substring(0, 37);
							s3 = s3 + "|" + afficher.getDroite().getNomJoueur();

							while (s3.length() < 56) {
								s3 = s3 + " ";
							}
							s3 = s3.substring(0, 56);
							s3 = s3 + "|";
							System.out.println(s3);

						}
					}
				}
			}
			System.out.println("|_______________||___________________|__________________|");

			// commencer choisir les places pour les cyclistes
			System.out.println("=================================================================");
			int place = 0;

			boolean occupe = false;
			boolean correct = false;
			int place1 = 0;
			Casse set1 = null;
			while (!occupe) {
				System.out.println(j.getNomJoueur() + " choisir la casse pour votre SPRINTER entre 0 et 4 s.v.p :");

				correct = false;
				while (!correct) {
					try {
						sc = new Scanner(System.in);
						place = sc.nextInt();
						correct = true;
						if (place < 0 || place > 4) {
							correct = false;
							throw new JeuException("tapez une entier entre 0 et 4 s.v.p :");

						}
					} catch (JeuException e) {
						System.out.println(e.getMessage());
					} catch (Exception e) {
						System.out.println("ce n'est pas une entier, tapez une entier s.v.p :");

					}
				}

				set1 = temp.get(place);
				System.out.println("tapez 0 pour mettre à gauche et 1 pour à droite s.v.p :");
				// choisir a gauche ou a droite
				correct = false;
				while (!correct) {
					try {
						sc = new Scanner(System.in);
						place1 = sc.nextInt();
						correct = true;
						if (place1 < 0 || place1 > 1) {
							correct = false;
							throw new JeuException("tapez une entier entre 0 et 1 s.v.p :");

						}
					} catch (JeuException e) {
						System.out.println(e.getMessage());
					} catch (Exception e) {
						System.out.println("ce n'est pas une entier, tapez une entier s.v.p :");

					}
				}
				occupe = true;
				try {
					if (place1 != 0) {
						if (set1.getDroite() != null) {
							occupe = false;
							throw new JeuException("position est deja occupe");

						}
					} else {
						if (set1.getGauche() != null) {
							occupe = false;
							throw new JeuException("position est deja occupe");
						}
					}
				} catch (JeuException e) {
					System.out.println(e.getMessage());
				}
			}
			if (place1 != 0) {

				j.setSprinter(t.getnomTuile(), place, true, this.avenue);
				Sprinter s = j.getSprinter();
				s.setAbsolute(avenue);
				j.setSprinter(s);
				set1.setDroite(s);
			} else {

				j.setSprinter(t.getnomTuile(), place, false, this.avenue);
				Sprinter s = j.getSprinter();
				s.setAbsolute(avenue);
				j.setSprinter(s);
				set1.setGauche(s);
			}
			temp.set(place, set1);
			// mettre rouleur a la place
			System.out.println("-----------------------------------------------------------------");

			correct = false;
			occupe = false;
			place = 0;
			Casse set2 = null;
			while (!occupe) {
				correct = false;
				System.out.println(j.getNomJoueur() + " choisir la casse pour votre ROULEUR entre 0 et 4 s.v.p :");

				while (!correct) {
					try {
						sc = new Scanner(System.in);
						place = sc.nextInt();
						correct = true;
						if (place < 0 || place > 4) {
							correct = false;
							throw new JeuException("tapez une entier entre 0 et 4 s.v.p :");

						}
					} catch (JeuException e) {
						System.out.println(e.getMessage());
					} catch (Exception e) {
						System.out.println("ce n'est pas une entier, tapez une entier s.v.p :");

					}
				}

				set2 = temp.get(place);
				System.out.println("tapez 0 pour mettre à gauche et 1 pour à droite s.v.p :");

				correct = false;
				while (!correct) {
					try {
						sc = new Scanner(System.in);
						place1 = sc.nextInt();
						correct = true;
						if (place1 < 0 || place1 > 1) {
							correct = false;
							throw new JeuException("tapez une entier entre 0 et 1 s.v.p :");

						}
					} catch (JeuException e) {
						System.out.println(e.getMessage());
					} catch (Exception e) {
						System.out.println("ce n'est pas une entier, tapez une entier s.v.p :");

					}
				}
				occupe = true;
				try {
					if (place1 != 0) {
						if (set2.getDroite() != null) {
							occupe = false;
							throw new JeuException("position est deja occupe");

						}
					} else {
						if (set2.getGauche() != null) {
							occupe = false;
							throw new JeuException("position est deja occupe");
						}
					}
				} catch (JeuException e) {
					System.out.println(e.getMessage());
				}
			}
			if (place1 != 0) {

				j.setRouleur(t.getnomTuile(), place, true, this.avenue);
				Rouleur r = j.getRouleur();
				r.setAbsolute(avenue);
				j.setRouleur(r);
				set2.setDroite(r);
			} else {
				j.setRouleur(t.getnomTuile(), place, false, this.avenue);
				Rouleur r = j.getRouleur();
				r.setAbsolute(avenue);
				j.setRouleur(r);
				set2.setGauche(r);
			}
			this.joueur.set(i, j);

			temp.set(place, set2);
			t.setTuilecase(temp);
			System.out.println("-----------------------------------------------------------------");
			System.out.println(j.getNomJoueur() + " a deja fait le choix!!!");
			System.out.println("=================================================================");

		}
		List<Tuile> p = avenue.getCircuitmain();
		p.set(0, t);
		avenue.setCircuitmain(p);

		t = avenue.tuilleALaPosition(0);
		// parcourir le list

		// initialiser une liste de case pour remplacer le tuile dans le avenue
		List<Casse> temp = t.getTuilecase();
		// 6 cases dans la tuile
		System.out.println("-----------------------------------------------------------------------------");
		System.out.println("| la condition des places occupees                                          |");
		System.out.println("| dans la tuile avec le nom 'a' qui est la tuile possedant une ligne depart |");
		System.out.println("| la ligne depart est entre casse 4 et casse 5                              |");
		System.out.println("-----------------------------------------------------------------------------");
		System.out.println(" _______________________________________________________");
		System.out.println("|case number    ||     gauche        |      doite       |");
		for (int m = 0; m < temp.size() - 1; m++) {
			// parcourir la liste des cases et verifier si c'est deja occupe
			Casse afficher = temp.get(m);
			String s = "|case " + m + "         || 0                 |0                 |";
			// condition:gauche et droit sont null
			if (afficher.getGauche() == null && afficher.getDroite() == null) {
				System.out.println(s);
			} else {
				// seulement la droite est null
				if (afficher.getGauche() == null) {
					String s1 = s.substring(0, 38);
					s1 = s1 + afficher.getDroite().getNomJoueur();
					while (s1.length() < 56) {
						s1 = s1 + " ";
					}
					s1 = s1 + "|";
					System.out.println(s1);
				} else {
					// la gauche est null
					if (afficher.getDroite() == null) {
						String s2 = s.substring(0, 18);
						s2 = s2 + afficher.getGauche().getNomJoueur();
						while (s2.length() < 37) {
							s2 = s2 + " ";
						}
						s2 = s2.substring(0, 37);
						s2 = s2 + "|0";
						while (s2.length() < 56) {
							s2 = s2 + " ";
						}
						s2 = s2 + "|";
						System.out.println(s2);
					} else {
						String s3 = s.substring(0, 18);
						s3 = s3 + afficher.getGauche().getNomJoueur();
						while (s3.length() < 37) {
							s3 = s3 + " ";
						}
						s3 = s3.substring(0, 37);
						s3 = s3 + "|" + afficher.getDroite().getNomJoueur();

						while (s3.length() < 56) {
							s3 = s3 + " ";
						}
						s3 = s3.substring(0, 56);
						s3 = s3 + "|";
						System.out.println(s3);

					}
				}
			}
		}
		System.out.println("|_______________||___________________|__________________|");
		System.out.println();
		System.out.println("tout est bien , alors on a deja determine tous les cyclistes dans ce jeu");
		System.out.println("Donc on commence les trois phases");
		System.out.println();

	}

	/**
	 * la partie principale pour demarrer le jeu
	 * 
	 */
	@SuppressWarnings("resource")
	public void demarrerJeu() {
		this.tour=0;
		for(int i=0;i<this.joueur.size();i++) {
			Joueur j=joueur.get(i);
			Rouleur r=j.getRouleur();
			r.insererCartes();
			Sprinter s=j.getSprinter();
			s.insererCartes();
			j.setRouleur(r);
			j.setSprinter(s);
			this.joueur.set(i, j);
		}
		boolean continuer = true;
		Scanner sc = new Scanner(System.in);
		while (!estFini() && continuer) {
			System.out.println();
			System.out.println("<<<<<<<<<<<<<<tour " + (this.tour + 1) + ">>>>>>>>>>>>>>");
			System.out.println();
			this.debutPhaseEnergie();
			this.passerPhaseSuivante();
			this.debutPhaseMouvement();
			this.passerPhaseSuivante();
			this.debutPhaseFinale();

			this.afficherPositionCycliste();
			System.out.println("=================================================================");
			System.out.println("Si vous voulez continuer tapez 'oui' si non tapez 'non' s.v.p :");
			String c = null;
			boolean correct = false;
			while (!correct) {
				try {
					sc = new Scanner(System.in);
					c = sc.next();
					correct = true;
					if (!c.equals("non") && !c.equals("0")) {
						correct = false;
						throw new JeuException("tapez 'oui' si non tapez 'non' s.v.p :");

					}
				} catch (JeuException e) {
					System.out.println(e.getMessage());
				}
			}
			System.out.println("=================================================================");
			if (c.equals("non")) {
				continuer = false;
			}
			this.tour++;
		}
		

	}

	/**
	 * methode permet les joueurs de choisir les cartes
	 */
	private void piocherCartes() {
		// parcourir la list de joueur pour piocher
		for (int i = 0; i < joueur.size(); i++) {
			Joueur j = joueur.get(i);
			Sprinter s = j.getSprinter();
			System.out.println("=================================================================");
			System.out.println(j.getNomJoueur() + "  pour votre SPRINTER");
			s.choisirCarte();

			j.setSprinter(s);

			Rouleur r = joueur.get(i).getRouleur();
			System.out.println("------------------------------------------------------------------");
			System.out.println(j.getNomJoueur() + "  pour votre ROULEUR");
			r.choisirCarte();
			j.setRouleur(r);
			joueur.set(i, j);
			System.out.println("=================================================================");
			System.out.println(j.getNomJoueur() + " a deja fait le choix pour les 2 cyclistes !!!");
			System.out.println("=================================================================");
		}
	}

	/**
	 * le methode pour avancer la cycliste
	 * 
	 * test passe
	 */
	private void avancerCycliste() {
		for (int i = 0; i < joueur.size(); i++) {

			Joueur j = joueur.get(i);
			Sprinter s = j.getSprinter();
			this.avenue = s.avancer(avenue);
			j.setSprinter(s);

			Rouleur r = j.getRouleur();
			this.avenue = r.avancer(avenue);
			j.setRouleur(r);
			joueur.set(i, j);
			System.out.println(j.getNomJoueur() + " avance !!!");

		}

	}

	/**
	 * le methode permet d'ajouter des cartes fatique dans la carte defausse
	 * 
	 * test passe
	 */
	private void appliquerFatique() {
		for (int i = 0; i < this.joueur.size(); i++) {
			Joueur j = this.joueur.get(i);
			Rouleur r = j.getRouleur();
			r.afficher();
			if (r.fatique(this.avenue)) {

				Carte c = this.carteFatiqueRoleur.remove(0);
				r.ajoutercarte(c);
			}
			Sprinter s = j.getSprinter();
			s.afficher();
			if (s.fatique(this.avenue)) {
				Carte c = this.carteFatiqueSprinter.remove(0);
				s.ajoutercarte(c);
			}
			j.setRouleur(r);
			j.setSprinter(s);
			this.joueur.set(i, j);
		}

	}

	/**
	 * vefifier si le jeu est fini; oui retourne vrai , sinon retourne faux
	 * 
	 * @return
	 */
	private boolean estFini() {
		boolean fini = false;
		List<Casse> t = this.avenue.tuilleALaPosition(this.avenue.getCircuitmain().size() - 1).getTuilecase();
		for (int i = 1; i < 6; i++) {
			Casse c = t.get(i);
			if (c.getDroite() != null || c.getGauche() != null) {
				fini = true;
			}
		}
		return fini;
	}

	/**
	 * ce methode retourne une liste trie
	 * 
	 * 
	 * @return
	 */
	private List<Cycliste> listeCyclistesTrierParPostion() {

		List<Cycliste> temp = new ArrayList<Cycliste>();
		List<Tuile> temp1 = avenue.getCircuitmain();
		for (int i = temp1.size() - 1; i >= 0; i--) {
			Tuile temp2 = temp1.get(i);
			List<Casse> temp3 = temp2.getTuilecase();
			for (int j = temp3.size() - 1; j >= 0; j--) {
				Casse c = temp3.get(j);
				if (c.getDroite() != null) {
					temp.add(c.getDroite());
					if (c.getGauche() != null) {
						temp.add(c.getGauche());
					}
				} else {
					if (c.getGauche() != null) {
						temp.add(c.getGauche());
					}
				}
			}
		}
		return temp;
	}

	/**
	 * retourne l'index du premiere cycliste
	 * @return
	 */
	public int retourneCyclisteEnTete() {

		List<Cycliste> l = this.listeCyclistesTrierParPostion();
		Cycliste c = l.get(0);
		int index=0;
		boolean trouve=false;
		while(!trouve) {
			if(this.joueur.get(index).getNomJoueur().equals(c.getNomJoueur())) {
				trouve=true;
			}else {
				index++;
			}
		}
		return index;
	}

	/**
	 * a la fin du jeu , chaque fois , on applique l'aspiration
	 */
	private void appliquerAspiration() {

		List<Cycliste> l = this.listeCyclistesTrierParPostion();

		List<Cycliste> temp = new ArrayList<Cycliste>();
		boolean changer = false;
		int i = l.size() - 1;
		while (i > 0) {
			changer = false;
			l = this.listeCyclistesTrierParPostion();
			if ((l.get(i - 1).getAbsolute() - l.get(i).getAbsolute()) == 0
					|| (l.get(i - 1).getAbsolute() - l.get(i).getAbsolute()) == 1) {
				temp.add(l.get(i));
			} else {
				if ((l.get(i - 1).getAbsolute() - l.get(i).getAbsolute()) == 2) {
					temp.add(l.get(i));
					changer = true;
					if (this.avenue.positionEnMontee(l.get(i - 1).getNom())
							|| this.avenue.positionEnMontee(l.get(i).getNom())) {
						changer = false;
						temp.clear();
					}
				} else {
					if ((l.get(i - 1).getAbsolute() - l.get(i).getAbsolute()) > 2) {
						temp.clear();
					}
				}

			}
			i--;
			if (changer && temp.size() != 0) {
				for (int j = temp.size() - 1; j >= 0; j--) {
					Cycliste temp4 = temp.get(j);
					int m = 0;
					boolean trouver = false;
					while (m < joueur.size() && !trouver) {
						if (joueur.get(m).getRouleur().equal(temp4)) {
							Joueur jr = joueur.get(m);
							this.avenue = temp4.avanceraspiration(avenue);
							jr.setRouleur((Rouleur) temp4);
							joueur.set(m, jr);
							trouver = true;

						} else {
							if (joueur.get(m).getSprinter().equal(temp4)) {
								Joueur jr = joueur.get(m);
								this.avenue = temp4.avanceraspiration(avenue);
								jr.setSprinter((Sprinter) temp4);
								joueur.set(m, jr);
								trouver = true;

							}

						}
						m++;

					}
				}
			}
		}
		System.out.println("l’aspiration est applique !!!");

	}

	private void passerPhaseSuivante() {
		System.out.println();
		System.out.println("=====On passe dans la phase suivante===========================");
		System.out.println();
	}

	private void debutPhaseEnergie() {
		System.out.println();
		System.out.println("=====le 1er phase : Phase d'energie=====");
		System.out.println(
				"Phase d’énergie : chacun pioche (simultanément) 4 cartes et en place une face cachée, pour un coureur, \n puis pour l’autre.");
		this.piocherCartes();
		System.out.println("=====le 1er phase est fini =====================================");
	}

	private void debutPhaseMouvement() {
		System.out.println();
		System.out.println("=====le 2eme phase : Phase de mouvement=====");
		System.out.println("Phase de mouvement : les cartes sont révélées et les coureurs se déplacent en fonction.");
		System.out.println("=================================================================");
		this.avancerCycliste();
		System.out.println("=====le 2eme phase est fini =====================================");
	}

	private void debutPhaseFinale() {
		System.out.println();
		System.out.println("=====le 3eme phase : Phase finale=====");
		System.out.println(
				"Phase finale : les cartes jouées sont retirées du jeu et on applique l’aspiration et la fatigue. ");
		System.out.println("=================================================================");
		System.out.println();
		System.out.println("-----Appliquez l’aspiration--------------------------------------");
		System.out.println();

		this.appliquerAspiration();
		System.out.println();
		System.out.println("-----Attribuez les cartes Fatigue--------------------------------");
		System.out.println();
		this.appliquerFatique();
		System.out.println();
		System.out.println("=====le 3eme phase est fini =====================================");

	}

	/**
	 * affiche les positions des cyclistes
	 */
	private void afficherPositionCycliste() {
		System.out.println();
		System.out.println("=====les resultats de ce tour=====");
		System.out.println(" G:gauche \n D:droite \n Nom:nom de tuile");
		System.out.println("__________________");
		System.out.println("Nom    || G | D ||");
		List<Tuile> temp1 = avenue.getCircuitmain();
		temp1 = avenue.getCircuitmain();
		for (int m = 0; m < temp1.size(); m++) {
			Tuile temp2 = temp1.get(m);
			List<Casse> temp3 = temp2.getTuilecase();

			for (int n = 0; n < temp3.size(); n++) {
				System.out.print("case " + temp2.getnomTuile() + " || ");
				Casse b = temp3.get(n);
				if (b.getGauche() == null) {
					System.out.print("0 |");
				} else {
					System.out.print("1 |");
				}
				if (b.getDroite() == null) {
					System.out.println(" 0 ||");
				} else {
					System.out.println(" 1 ||");
				}

			}
		}
		System.out.println("------------------");
		System.out.println(" _____________________________________________________________________________________");
		System.out.println("|nom du joueur |type du cycliste |nom du tuile |casse numero |file  |position absolute|");
		List<Cycliste> l = this.listeCyclistesTrierParPostion();
		for (int m = 0; m < l.size(); m++) {
			System.out.println("|--------------|-----------------|-------------|-------------|------|-----------------|");
			String s = "|" + l.get(m).getNomJoueur();
			while (s.length() < 15) {
				s = s + " ";
			}
			s = s.substring(0, 15);
			s = s + "|";
			Cycliste c = l.get(m);
			if (c instanceof Rouleur) {
				s = s + "Rouleur";
			} else {
				if (c instanceof Sprinter) {
					s = s + "Sprinter";
				}
			}
			while (s.length() < 33) {
				s = s + " ";
			}
			s = s.substring(0, 33);
			s = s + "|" + l.get(m).getNom();
			while (s.length() < 47) {
				s = s + " ";
			}
			s = s.substring(0, 47);
			s = s + "|" + l.get(m).getPositionActuelle();
			while (s.length() < 61) {
				s = s + " ";
			}
			s = s.substring(0, 61) + "|";
			if (l.get(m).isFileDroite()) {
				s = s + "droite";
			} else {
				s = s + "gauche";
			}
			s = s + "|" + l.get(m).getAbsolute();
			while (s.length() < 86) {
				s = s + " ";
			}
			s = s.substring(0, 86) + "|";
			System.out.println(s);
		}
		System.out.println("|______________|_________________|_____________|_____________|______|_________________|");
	}

	/**
	 * retoune la liste de joueur
	 * 
	 * @return
	 */
	public List<Joueur> getJoueur() {
		return joueur;
	}


	/**
	 * @return
	 */
	public Circuit getAvenue() {
		return avenue;
	}

	/**
	 * @return
	 */
	public int getTour() {
		return tour;
	}
	
	
	
	public String toString() {
		String status=" _____________________________________________________________________________________"+"\n";
		status=status+"|nom du joueur |type du cycliste |nom du tuile |casse numero |file  |position absolute|"+"\n";
		List<Cycliste> l = this.listeCyclistesTrierParPostion();
		for (int m = 0; m < l.size(); m++) {
			status=status+"|--------------|-----------------|-------------|-------------|------|-----------------|"+"\n";
			String s = "|" + l.get(m).getNomJoueur();
			while (s.length() < 15) {
				s = s + " ";
			}
			s = s.substring(0, 15);
			s = s + "|";
			Cycliste c = l.get(m);
			if (c instanceof Rouleur) {
				s = s + "Rouleur";
			} else {
				if (c instanceof Sprinter) {
					s = s + "Sprinter";
				}
			}
			while (s.length() < 33) {
				s = s + " ";
			}
			s = s.substring(0, 33);
			s = s + "|" + l.get(m).getNom();
			while (s.length() < 47) {
				s = s + " ";
			}
			s = s.substring(0, 47);
			s = s + "|" + l.get(m).getPositionActuelle();
			while (s.length() < 61) {
				s = s + " ";
			}
			s = s.substring(0, 61) + "|";
			if (l.get(m).isFileDroite()) {
				s = s + "droite";
			} else {
				s = s + "gauche";
			}
			s = s + "|" + l.get(m).getAbsolute();
			while (s.length() < 86) {
				s = s + " ";
			}
			s = s.substring(0, 86) + "|";
			status=status+s+"\n";
		}
		status=status+"|______________|_________________|_____________|_____________|______|_________________|";
		return status;
	}

}
