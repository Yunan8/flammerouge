package Projet;

import java.io.*;
import java.util.*;

/**
* @author HOU Yunan
* 
*/
public class JeuHistorique {
	private List<Joueur> joueur;
	private Circuit avenue;
	private int tour;
	private List<Integer> score=new ArrayList<Integer>();;
	private final static String txt="JeuHistorique.txt";
	/**
	 * constuire une historique
	 * @param j
	 */
	public JeuHistorique() {
		for(int i=0;i<4;i++) {
			score.add(0);
		}
	}
	/**
	 * permet mise a jour
	 * @param j
	 */
	public void addTour(Jeu j) {
		tour=j.getTour();
	}
	/**
	 * ajouter les resultats
	 * @param s
	 * @param src
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void remplitFichier(String s,String src) throws IOException, FileNotFoundException  
	{  
		BufferedWriter out=new BufferedWriter( new FileWriter(src));
		for(int i=0;i<this.joueur.size();i++) {
			out.write("le score de "+this.joueur.get(i).getNomJoueur()+" est "+this.score.get(i));
			out.newLine();
		}
		out.write("le nom de avenue "+this.avenue.getNomCircuit());
		out.write("le nombre de tour est "+this.tour); 
		 out.newLine(); 
		out.write(s);
		 out.newLine();  
		 out.close();
		 
	}
	/**
	 * afficher les historiques
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void extraitFichier() throws IOException, FileNotFoundException {
		String line;
		BufferedReader in = new BufferedReader(   new FileReader(txt));
		while ((line = in.readLine()) != null) {
			System.out.println(line);
			
		}	
		in.close();
	}
	/**
	 * le methode permet d'afficher 
	 */
	public void afficher() {
		try {
			this.extraitFichier();
			}catch (FileNotFoundException e) {
				System.out.println("fichier non trouve");
			} catch (IOException e) {
				System.out.println("Erreur d'entree / sortie ");
			}
	}
	public void restaurer(Jeu j) {
		this.joueur=j.getJoueur();
		this.avenue=j.getAvenue();
		this.addTour(j);
		score.set(j.retourneCyclisteEnTete(),score.get(j.retourneCyclisteEnTete())+1);
		try {
		this.remplitFichier(j.toString(), txt);
		}catch (FileNotFoundException e) {
			System.out.println("fichier non trouve");
		} catch (IOException e) {
			System.out.println("Erreur d'entree / sortie ");
		}
	}
}
