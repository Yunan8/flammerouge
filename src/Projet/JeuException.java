package Projet;

/**
 * @author HOU Yunan
 * 
 */

@SuppressWarnings("serial")
public class JeuException extends Exception {
	public JeuException() {
		super();
	}

	public JeuException(String s) {
		super(s);
	}
}
