package Projet;
/**
* @author HOU Yunan
* 
*/
public class Casse {
	private Cycliste gauche;
	private Cycliste droite;

	/**
	 * construeur pour une casse
	 */
	public Casse() {
		gauche = null;
		droite = null;
	}

	/**
	 * retourne la cycliste qui est dans la gauche
	 * @return
	 */
	public Cycliste getGauche() {
		return gauche;
	}

	/**
	 * changer la cycliste qui est dans la gauche 
	 * @param gauche
	 */
	public void setGauche(Cycliste gauche) {
		this.gauche = gauche;
	}

	/**
	 * retourne la cycliste qui est dans la droite
	 * @return
	 */
	public Cycliste getDroite() {
		return droite;
	}

	/**
	 * changer la cycliste qui est dans la droite 
	 * @param droite
	 */
	public void setDroite(Cycliste droite) {
		this.droite = droite;
	}

	/**
	 * effacer la cycliste qui est dans la droite 
	 */
	public void clearDroite() {
		this.droite = null;

	}

	/**
	 * effacer la cycliste qui est dans la gauche 
	 */
	public void clearGauche() {
		this.gauche = null;

	}

}
