package Projet;

/**
 * @author HOU Yunan
 * 
 */
public class Joueur {

	private String nomJoueur;
	private Rouleur r;
	private Sprinter s;

	/**
	 * initialiser les joueurs
	 */
	public Joueur() {
		nomJoueur=null;
		r = new Rouleur();
		s = new Sprinter();
	}

	/**
	 * retourne le nom de joueur
	 * 
	 * @return
	 */
	public String getNomJoueur() {
		return nomJoueur;
	}

	/**
	 * changer le nom de joueur
	 * 
	 * @param nomJoueur
	 */
	public void setNomJoueur(String nomJoueur) {
		this.r.setNomJoueur(nomJoueur);
		this.s.setNomJoueur(nomJoueur);
		this.nomJoueur = nomJoueur;
	}

	/**
	 * changer le rouleur
	 * 
	 * @param rl
	 */
	public void setRouleur(Rouleur rl) {
		this.r = rl;
	}

	/**
	 * changer le sprinter
	 * 
	 * @param sp
	 */
	public void setSprinter(Sprinter sp) {
		this.s = sp;
	}

	/**
	 * retourne le rouleur
	 * 
	 * @return
	 */
	public Rouleur getRouleur() {
		return this.r;
	}

	/**
	 * changer le rouleur
	 * 
	 * @param n
	 * @param p
	 * @param d
	 * @param c
	 */
	public void setRouleur(String n, int p, boolean d, Circuit c) {
		this.r.setposition(n, p, d, c);
	}

	/**
	 * changer le sprinter
	 * 
	 * @return
	 */
	public Sprinter getSprinter() {
		return this.s;
	}

	/**
	 * changer le sprinter
	 * 
	 * @param n
	 * @param p
	 * @param d
	 * @param c
	 */
	public void setSprinter(String n, int p, boolean d, Circuit c) {
		this.s.setposition(n, p, d, c);

	}

}
