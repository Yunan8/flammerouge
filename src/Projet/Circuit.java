package Projet;

import java.util.ArrayList;
import java.util.List;

public class Circuit {
	private String nomCircuit;
	private List<Tuile> circuitmain;

	/**
	 * constructeur pour une circuit
	 * 
	 * @param nom
	 */
	public Circuit(String nom) {
		this.nomCircuit = nom;
		circuitmain = new ArrayList<Tuile>();
	}

	/**
	 * mettre les differentes tuiles dans une circuit
	 */
	public void setCircuit() {
		if (this.nomCircuit.equals("Avenue Corso Paseo")) {
			circuitmain.add(new TuileLigneDroite("a", 1, 0));
			circuitmain.add(new TuileLigneDroite("b", 0, 0));
			circuitmain.add(new TuileLigneDroite("c", 0, 0));
			circuitmain.add(new TuileLigneDroite("d", 0, 0));
			circuitmain.add(new TuileVirage("e", 1, 0));
			circuitmain.add(new TuileLigneDroite("f", 0, 1));
			circuitmain.add(new TuileVirage("g", 1, 0));
			circuitmain.add(new TuileVirage("h", 0, 0));
			circuitmain.add(new TuileVirage("i", 1, 1));
			circuitmain.add(new TuileVirage("j", 1, -1));
			circuitmain.add(new TuileVirage("k", 0, 0));
			circuitmain.add(new TuileLigneDroite("l", 0, 1));
			circuitmain.add(new TuileLigneDroite("m", 0, 0));
			circuitmain.add(new TuileLigneDroite("n", 0, -1));
			circuitmain.add(new TuileVirage("o", 1, 0));
			circuitmain.add(new TuileVirage("p", 1, 1));
			circuitmain.add(new TuileVirage("q", 0, 0));
			circuitmain.add(new TuileVirage("r", 0, -1));
			circuitmain.add(new TuileVirage("s", 0, 0));
			circuitmain.add(new TuileVirage("t", 0, 0));
			circuitmain.add(new TuileLigneDroite("u", -1, 0));

		}
	}

	/**
	 * retourne dans la liste de tuile la position n
	 * 
	 * @param n
	 * @return
	 */
	public Tuile tuilleALaPosition(int n) {
		return this.circuitmain.get(n);
	}

	/**
	 * verifier si la tuile est descente ou non
	 * 
	 * @param index
	 * @return
	 */
	public boolean positionEnDescente(int index) {

		return this.circuitmain.get(index).estDescente();
	}

	/**
	 * verifier si la tuile est montee ou non
	 * 
	 * @param nom
	 * @return
	 */
	public boolean positionEnMontee(String nom) {
		int index = 0;
		boolean trouve = false;
		while (index < this.circuitmain.size() && !trouve) {
			if (this.circuitmain.get(index).getnomTuile().equals(nom)) {
				trouve = true;
			} else {
				index++;
			}

		}
		return this.circuitmain.get(index).estMontee();
	}

	/**
	 * verifier si la tuile est normale ou non
	 * 
	 * @param index
	 * @return
	 */
	public boolean positionEnVoieNormale(int index) {
		return this.circuitmain.get(index).estLigneDroite();
	}

	/**
	 * retourne la liste de circuite
	 * 
	 * @return
	 */
	public List<Tuile> getCircuitmain() {
		return circuitmain;
	}

	/**
	 * remplace la liste avec une nouvelle liste de circuit
	 * 
	 * @param circuitmain
	 */
	public void setCircuitmain(List<Tuile> circuitmain) {
		this.circuitmain = circuitmain;
	}

	/**
	 * retourne le cycliste qui possede une position exacte
	 * 
	 * @param index
	 * @param positionrelative
	 * @param fileDroite
	 * @return
	 */
	public Cycliste getCycliste(int index, int positionrelative, boolean fileDroite) {
		if (fileDroite) {
			return circuitmain.get(index).getTuilecase().get(positionrelative).getDroite();
		} else {
			return circuitmain.get(index).getTuilecase().get(positionrelative).getGauche();
		}
	}

	/**
	 * set une cycliste avec une position exacte
	 * 
	 * @param index
	 * @param positionrelative
	 * @param fileDroite
	 * @param c
	 */
	public void setCycliste(int index, int positionrelative, boolean fileDroite, Cycliste c) {
		Tuile temp = circuitmain.get(index);
		List<Casse> tuiletemp = temp.getTuilecase();
		Casse temp1 = tuiletemp.get(positionrelative);
		if (fileDroite) {
			temp1.setDroite(c);
		} else {
			temp1.setGauche(c);
		}
		tuiletemp.set(positionrelative, temp1);
		temp.setTuilecase(tuiletemp);
		circuitmain.set(index, temp);
	}

	/**
	 * retourne le nom de circuit
	 * 
	 * @return
	 */
	public String getNomCircuit() {
		return nomCircuit;
	}

	/**
	 * effacer la cycliste dans la position exacte
	 * 
	 * @param index
	 * @param positionrelative
	 * @param fileDroite
	 */
	public void clearCycliste(int index, int positionrelative, boolean fileDroite) {
		Tuile temp = circuitmain.get(index);
		List<Casse> tuiletemp = temp.getTuilecase();
		Casse temp1 = tuiletemp.get(positionrelative);
		if (fileDroite) {
			temp1.clearDroite();
		} else {
			temp1.clearGauche();
		}
		tuiletemp.set(positionrelative, temp1);
		temp.setTuilecase(tuiletemp);
		circuitmain.set(index, temp);
	}
}
