package Projet;

/**
 * @author HOU Yunan
 * 
 */
public class TuileLigneDroite extends Tuile {
	public int fonction;

	/**
	 * construteur permet d'ajouer des casses
	 * 
	 * @param nom
	 * @param n
	 * @param m
	 */
	public TuileLigneDroite(String nom, int n, int m) {
		super(nom, n, m);
		nombreDeCasses = 6;
		fonction = n;
		for (int i = 0; i < nombreDeCasses; i++) {
			tuilecase.add(new Casse());
		}

	}

	/**
	 * si c'est une ligne depart
	 * 
	 * @return
	 */
	public boolean estLigneDepart() {
		return (fonction == 1);
	}

	/**
	 * si c'est une ligne d'arrive
	 * 
	 * @return
	 */
	public boolean estLigneArrive() {
		return (fonction == -1);
	}

	/**
	 * si c'est une ligne normale
	 * 
	 * @return
	 */
	public boolean estLigneNormale() {
		return (fonction == 0);
	}
}
