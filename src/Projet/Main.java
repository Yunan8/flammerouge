package Projet;

import java.util.Scanner;

/**
 * @author HOU Yunan
 * 
 */
public class Main {

	/**
	 * jeu principale
	 * 
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(" ___________________________________");
		System.out.println("|                                   |");
		System.out.println("|     Bienvenu  a  Flamme Rouge     |");
		System.out.println("|                                   |");
		System.out.println("|___________________________________|");
		System.out.println();
		System.out.println("---Introduction---");
		System.out.println();
		System.out.println(" __La grande course_________________________");
		System.out.println("|Dans le monde du cyclisme, la flamme       |");
		System.out.println("|rouge est un drapeau qui indique le        |");
		System.out.println("|dernier kilometre : la derniere ligne      |");
		System.out.println("|droite, le moment ou il faut tout donner ! |");
		System.out.println("|Ce matin, la banlieue de Paris fourmille   |");
		System.out.println("|de cyclistes venus du monde entier pour    |");
		System.out.println("|disputer une course memorable, sous        |");
		System.out.println("|les yeux de nombreux spectateurs. Qui      |");
		System.out.println("|passera la ligne d’arrivee le premier      |");
		System.out.println("|et inscrira son nom dans l’Histoire ?      |");
		System.out.println("|Strategie et endurance seront les cles     |");
		System.out.println("|de la victoire. Que la meilleure equipe    |");
		System.out.println("|gagne!_____________________________________|");
		System.out.println();
		System.out.println("Pour commencer notre jeu , il reste quelques choses a faire ");
		System.out.println();


		JeuHistorique jh = new JeuHistorique();
		Jeu j = new Jeu();
		boolean ct = false;
		Scanner sc = new Scanner(System.in);
		while (!ct) {
			
			j.positionDepart();
			j.demarrerJeu();
			jh.restaurer(j);
			jh.afficher();
			System.out.println("=================================================================");
			System.out.println("Si vous voulez continuer de ce jeu ,tapez 'oui' si non tapez 'non' s.v.p :");
			String c = null;
			boolean correct = false;
			while (!correct) {
				try {
					sc = new Scanner(System.in);
					c = sc.next();
					correct = true;
					if (!c.equals("non") && !c.equals("oui")) {
						correct = false;
						throw new JeuException("tapez 'oui' si non tapez 'non' s.v.p :");

					}
				} catch (JeuException e) {
					System.out.println(e.getMessage());
				}
			}
			System.out.println("=================================================================");
			if (c.equals("non")) {
				ct = false;
			}
		}

	}

}
