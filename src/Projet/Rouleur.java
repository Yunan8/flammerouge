package Projet;

import java.util.ArrayList;

/**
 * @author HOU Yunan
 * 
 */
public class Rouleur extends Cycliste {
	/**
	 * construteur
	 */
	public Rouleur() {
		super();
		this.insererCartes();
	}

	public void insererCartes() {
		carte = new ArrayList<Carte>();
		for (int i = 0; i < 3; i++) {
			carte.add(new CarteEnergie(3));
			carte.add(new CarteEnergie(4));
			carte.add(new CarteEnergie(5));
			carte.add(new CarteEnergie(6));
			carte.add(new CarteEnergie(7));
		}
		// deranger les cartes
		for (int i = 14; i > 0; i--) {
			int k = (int) (Math.random() * (i + 1));
			Carte temp = carte.get(i);
			carte.set(i, carte.get(k));
			carte.set(k, temp);
		}
	}

	@Override
	public String toString() {
		return "Rouleur [getNomJoueur()=" + getNomJoueur() + ", getNom()=" + getNom() + ", isFileDroite()="
				+ isFileDroite() + ", getPositionActuelle()=" + getPositionActuelle() + ", getAbsolute()="
				+ getAbsolute() + "]";
	}
	
}
