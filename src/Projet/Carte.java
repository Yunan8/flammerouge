package Projet;

public abstract class Carte {
	protected int numero;

	public Carte(int n) {
		this.numero = n;
	}

	public int getNumero() {
		return numero;
	}

	@Override
	public abstract String toString();
	

}
