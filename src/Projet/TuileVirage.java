package Projet;

/**
 * @author HOU Yunan
 * 
 */
public class TuileVirage extends Tuile {
	public int serre;

	/**
	 * construteur permet d'ajouter des casses
	 * 
	 * @param nom
	 * @param n
	 * @param m
	 */
	public TuileVirage(String nom, int n, int m) {
		super(nom, n, m);
		serre = n;
		nombreDeCasses = 2;
		for (int i = 0; i < nombreDeCasses; i++) {
			tuilecase.add(new Casse());
		}
	}

	/**
	 * si c'est une tuile leger
	 * 
	 * @return
	 */
	public boolean estLeger() {
		return (serre == 0);
	}

	/**
	 * si ce'st une tuile serre
	 * 
	 * @return
	 */
	public boolean estSerre() {
		return (serre == 1);
	}
}
