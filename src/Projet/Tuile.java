package Projet;

import java.util.*;

/**
 * @author HOU Yunan
 * 
 */
public class Tuile {
	private int pente;
	public int nombreDeCasses;
	private String nomTuile;
	public List<Casse> tuilecase;

	/**
	 * construteur
	 * 
	 * @param nom
	 *            nom de joueur
	 * @param n
	 *            fonction ou type de tuile
	 * @param m
	 *            le pente
	 */
	public Tuile(String nom, int n, int m) {
		this.nomTuile = nom;
		this.pente = m;
		tuilecase = new ArrayList<Casse>();
	}

	/**
	 * si le tuile est montee
	 * 
	 * @return
	 */
	public boolean estMontee() {
		return (this.pente == 1);
	}

	/**
	 * si le tuile est descente
	 * 
	 * @return
	 */
	public boolean estDescente() {
		return (this.pente == -1);
	}

	public boolean estLigneDroite() {
		return (this.pente == 0);
	}

	/**
	 * retourne le nom de tuile
	 * 
	 * @return
	 */
	public String getnomTuile() {
		return this.nomTuile;
	}

	/**
	 * retourne la liste de casse
	 * 
	 * @return
	 */
	public List<Casse> getTuilecase() {
		return tuilecase;
	}

	/**
	 * changer la liste de casse
	 * 
	 * @param tuilecase
	 */
	public void setTuilecase(List<Casse> tuilecase) {
		this.tuilecase = tuilecase;
	}

}
